package at.marinica.mycalendar;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.PointsGraphSeries;

import java.util.Date;
import java.util.List;

import at.marinica.mycalendar.database.DateUtil;
import at.marinica.mycalendar.database.MyCalendarViewModel;
import at.marinica.mycalendar.database.Period;
import at.marinica.mycalendar.database.User;
import at.marinica.mycalendar.database.Weight;


public class WeightFragment extends Fragment {

    private static final String TAG = "WeightFragment";

    private MyCalendarViewModel myCalendarViewModel;
    PointsGraphSeries<DataPoint> series = new PointsGraphSeries<>();
    GraphView graph;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_weight, container, false);
        graph = (GraphView)view.findViewById(R.id.weightgraph);

        myCalendarViewModel = ViewModelProviders.of(this).get(MyCalendarViewModel.class);

        TabbedActivity tabbedActivity = (TabbedActivity) getActivity();
        if (tabbedActivity != null) {
            tabbedActivity.addUserSelectionChangedListener(this::update);
        }

        myCalendarViewModel.getAllWeights().observe(this, weights -> {
            if (tabbedActivity != null) {
                update(tabbedActivity.getCurrentUser());
            }
        });

        myCalendarViewModel.getAllPeriodsFromAllUsers().observe(this, periods -> {
            if (tabbedActivity != null) {
                update(tabbedActivity.getCurrentUser());
            }
        });

        // custom label formatter to show currency "kg"
        graph.getGridLabelRenderer().setLabelFormatter(new DateAndYLabelFormatter(getActivity()," kg"));
        graph.addSeries(series);
        // set date label formatter
        graph.getGridLabelRenderer().setPadding(45);
        graph.getGridLabelRenderer().setHorizontalLabelsAngle(135);
        graph.getGridLabelRenderer().setHumanRounding(true);
        update(tabbedActivity.getCurrentUser());
        return view;
    }

    private void update(User newUser) {
        Log.d(TAG, "entered update");
        if (newUser == null) {
            Log.d(TAG, "newUser is null");
            return;
        }
        Period lastPeriod = myCalendarViewModel.getLastPeriodForUser(newUser.getId());
        if (lastPeriod == null) {
            //TODO no period is there yet - what do we do? Error message? Display something differently?
            Log.d(TAG, "no period");
            return;
        }
        List<Weight> weightsFromUserInTimeframe = myCalendarViewModel.getWeightsFromUserInTimeframe(newUser, lastPeriod.getStartDate());
        for (Weight w : weightsFromUserInTimeframe) {
            Log.d(TAG, String.format("Found temperature: id %d, userId %d, date %d, value %f", w.getId(), w.getUserId(), w.getDate(), w.getValue()));
        }
        DataPoint[] weightDataPoints = weightsFromUserInTimeframe.stream().map(w-> new DataPoint(new Date(w.getDate()), w.getValue())).toArray(DataPoint[]::new);
        graph.getViewport().setMinX(lastPeriod.getStartDate());
        graph.getViewport().setMaxX(DateUtil.truncateTimestamp(System.currentTimeMillis()));
        graph.getViewport().setXAxisBoundsManual(true);
        Log.d(TAG, "Array length: " + weightDataPoints.length);
        graph.getGridLabelRenderer().setLabelFormatter(new DateAndYLabelFormatter(getActivity()," kg"));
        series.resetData(weightDataPoints);
    }

}
