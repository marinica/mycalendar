package at.marinica.mycalendar;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.PointsGraphSeries;

import java.util.Date;
import java.util.List;

import at.marinica.mycalendar.database.DateUtil;
import at.marinica.mycalendar.database.MyCalendarViewModel;
import at.marinica.mycalendar.database.Period;
import at.marinica.mycalendar.database.Temperature;
import at.marinica.mycalendar.database.User;

public class TemperatureFragment extends Fragment {

    private static final String TAG = "TemperatureFragment";

    private MyCalendarViewModel myCalendarViewModel;
    PointsGraphSeries<DataPoint> series = new PointsGraphSeries<>();
    GraphView graph;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_temperature, container, false);

        myCalendarViewModel = ViewModelProviders.of(this).get(MyCalendarViewModel.class);
        graph = (GraphView)view.findViewById(R.id.tempgraph);
        TabbedActivity tabbedActivity = (TabbedActivity) getActivity();
        if (tabbedActivity != null) {
            tabbedActivity.addUserSelectionChangedListener(this::update);
        }

        myCalendarViewModel.getAllTemperatures().observe(this, temperatures -> {
            if (tabbedActivity != null) {
                update(tabbedActivity.getCurrentUser());
            }
        });

        myCalendarViewModel.getAllPeriodsFromAllUsers().observe(this, periods -> {
            if (tabbedActivity != null) {
                update(tabbedActivity.getCurrentUser());
            }
        });

        // custom label formatter to show currency "°C"
        graph.getGridLabelRenderer().setLabelFormatter(new DateAndYLabelFormatter(getActivity()," °C"));

        graph.addSeries(series);
        // set date label formatter
        //graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getActivity()));
        graph.getGridLabelRenderer().setPadding(60);
        graph.getGridLabelRenderer().setHorizontalLabelsAngle(135);
        graph.getGridLabelRenderer().setHumanRounding(true);
        update(tabbedActivity.getCurrentUser());
        return view;
    }


    private void update(User newUser) {
        Log.d(TAG, "entered update");
        if (newUser == null) {
            Log.d(TAG, "newUser is null");
            return;
        }
        Period lastPeriod = myCalendarViewModel.getLastPeriodForUser(newUser.getId());
        if (lastPeriod == null) {
            //TODO no period is there yet - what do we do? Error message? Display something differently?
            Log.d(TAG, "no period");
            return;
        }
        List<Temperature> temperaturesFromUserInTimeframe = myCalendarViewModel.getTemperaturesFromUserInTimeframe(newUser, lastPeriod.getStartDate());
        for (Temperature t : temperaturesFromUserInTimeframe) {
            Log.d(TAG, String.format("Found temperature: id %d, userId %d, date %d, value %f", t.getId(), t.getUserId(), t.getDate(), t.getValue()));
        }
        DataPoint[] tempDataPoints = temperaturesFromUserInTimeframe.stream().map(t-> new DataPoint(new Date(t.getDate()), t.getValue())).toArray(DataPoint[]::new);
        graph.getViewport().setMinX(lastPeriod.getStartDate());
        graph.getViewport().setMaxX(DateUtil.truncateTimestamp(System.currentTimeMillis()));
        graph.getViewport().setXAxisBoundsManual(true);
        Log.d(TAG, "Array length: " + tempDataPoints.length);
        series.resetData(tempDataPoints);
    }


}
