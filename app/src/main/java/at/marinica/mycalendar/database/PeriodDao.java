package at.marinica.mycalendar.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface PeriodDao {

    @Insert
    void insert(Period period);

    @Query("DELETE FROM period")
    void deleteAll();

    @Delete
    void delete(Period... periods);

    @Update
    void update(Period... periods);

    @Query("SELECT * from period WHERE userId = :userId")
    LiveData<List<Period>> getPeriodsFromUser(int userId);

    @Query("SELECT * from period WHERE userId = :userId ORDER BY startDate DESC LIMIT 1")
    Period getLastPeriodForUser(int userId);

    @Query("SELECT * from period")
    LiveData<List<Period>> getAllPeriods();

    @Query("SELECT * from period WHERE userId = :userId ORDER BY startDate DESC LIMIT :limit")
    List<Period> getLastPeriodsForUser(int userId, int limit);
}
