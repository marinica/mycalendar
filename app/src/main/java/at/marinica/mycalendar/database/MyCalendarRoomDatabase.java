package at.marinica.mycalendar.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

@Database(entities = {User.class, Period.class, Temperature.class, Note.class, Weight.class}, version = 5)
public abstract class MyCalendarRoomDatabase extends RoomDatabase {

    public abstract UserDao userDao();

    public abstract PeriodDao periodDao();

    public abstract TemperatureDao temperatureDao();

    public abstract NoteDao noteDao();

    public abstract WeightDao weightDao();

    private static volatile MyCalendarRoomDatabase INSTANCE;

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final UserDao mDao;
        private final TemperatureDao temperatureDao;
        private final NoteDao noteDao;
        private final PeriodDao periodDao;
        private final WeightDao weightDao;

        PopulateDbAsync(MyCalendarRoomDatabase db) {
            mDao = db.userDao();
            temperatureDao = db.temperatureDao();
            noteDao = db.noteDao();
            periodDao = db.periodDao();
            weightDao = db.weightDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {
//            mDao.deleteAll();
//            User user1 = new User(1, "Barbara");
//            mDao.insert(user1);
//            temperatureDao.deleteAll();
//            Temperature temperature = new Temperature(user1.getId(), System.currentTimeMillis(), 37.0);
//            temperatureDao.insert(temperature);
//            User user2 = new User(2, "Christina");
//            mDao.insert(user2);
//            User user3 = new User(3, "Test");
//            mDao.insert(user3);
//            periodDao.deleteAll();
            return null;
        }
    }

    static MyCalendarRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MyCalendarRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MyCalendarRoomDatabase.class, "mycalendar_database")
                            .fallbackToDestructiveMigration()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

}
