package at.marinica.mycalendar.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface WeightDao {

    @Insert
    void insert(Weight weight);

    @Query("DELETE FROM weight")
    void deleteAll();

    @Delete
    void delete(Weight... weights);

    @Update
    void update(Weight... weights);

    @Query("SELECT * from weight WHERE userId = :userId AND date >= :startDate order by date")
    List<Weight> getWeightsFromUserInTimeframe(int userId, long startDate);

    @Query("SELECT * from weight WHERE userId = :userId AND date = :date")
    Weight getWeightForExactDate(int userId, long date);

    @Query("SELECT * from weight")
    LiveData<List<Weight>> getAllWeights();

}
