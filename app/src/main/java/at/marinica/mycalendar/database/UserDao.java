package at.marinica.mycalendar.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface UserDao {

    @Insert
    void insert(User user);

    @Query("DELETE FROM user")
    void deleteAll();

    @Query("SELECT * from user ORDER BY name ASC")
    LiveData<List<User>> getAllUsers();

    @Update
    void update(User... users);

    @Delete
    void deleteUsers(User... users);

    @Query("SELECT * from user WHERE id = :userId")
    User getUserById(int userId);
}
