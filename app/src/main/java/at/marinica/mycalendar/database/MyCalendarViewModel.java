package at.marinica.mycalendar.database;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

public class MyCalendarViewModel extends AndroidViewModel {

    private MyCalendarRepository mRepository;

    private LiveData<List<User>> mAllUsers;
    private LiveData<List<Note>> mAllNotes;
    private LiveData<List<Period>> mAllPeriods;
    private LiveData<List<Temperature>> mAllTemperatures;
    private LiveData<List<Weight>> mAllWeights;


    public MyCalendarViewModel(Application application) {
        super(application);
        mRepository = new MyCalendarRepository(application);
        mAllUsers = mRepository.getAllUsers();
        mAllNotes = mRepository.getAllNotes();
        mAllPeriods = mRepository.getAllPeriods();
        mAllTemperatures = mRepository.getAllTemperatures();
        mAllWeights = mRepository.getAllWeights();
    }

    public LiveData<List<User>> getAllUsers() { return mAllUsers; }

    public User getUserById(int id) {
        return mRepository.getUserById(id);
    }

    public void insertUser(User user) { mRepository.insertUser(user); }

    public void deleteUser(User user) { mRepository.deleteUser(user); }

    public void updateUser(User user) {
        mRepository.updateUser(user);
    }

    public LiveData<List<Temperature>> getAllTemperatures() { return mAllTemperatures; }

    public List<Temperature> getTemperaturesFromUserInTimeframe(User user, long startDate) {
        return mRepository.getTemperaturesFromUserInTimeframe(user, startDate);
    }

    public Temperature getTemperatureFromUserOnDate(int userId, long date) {
        return mRepository.getTemperatureFromUserOnDate(userId, date);
    }

    public void insertTemperature(Temperature temperature) {
        mRepository.insertTemperature(temperature);
    }

    public void updateTemperature(Temperature temperature) {
        mRepository.updateTemperature(temperature);
    }

    public void deleteTemperature(Temperature temperature) {
        mRepository.deleteTemperature(temperature);
    }

    public Period getLastPeriodForUser(int userId) {
        return mRepository.getLastPeriodForUser(userId);
    }

    public List<Period> getLastPeriodsForUser(int userId, int limit) {
        return mRepository.getLastPeriodsForUser(userId, limit);
    }

    public void insertPeriod(Period period) {
        mRepository.insertPeriod(period);
    }

    public void updatePeriod(Period period) {
        mRepository.updatePeriod(period);
    }

    public void deletePeriod(Period period) {
        mRepository.deletePeriod(period);
    }

    public LiveData<List<Period>> getAllPeriodsFromAllUsers() { return mAllPeriods; }

    public void insertNote(Note note) { mRepository.insertNote(note); }

    public void deleteNote(Note note) { mRepository.deleteNote(note); }

    public void updateNote(Note note) {
        mRepository.updateNote(note);
    }

    public Note getNoteByDate(int userId, long date) {
        return mRepository.getNoteByDate(userId, date);
    }

    public LiveData<List<Note>> getAllNotesFromAllUsers(){
        //return mRepository.getAllNotes().getValue();
        return mAllNotes; //mRepository.getAllNotes().getValue();
    }

    public LiveData<List<Weight>> getAllWeights() { return mAllWeights; }

    public List<Weight> getWeightsFromUserInTimeframe(User user, long startDate) {
        return mRepository.getWeightsFromUserInTimeframe(user, startDate);
    }

    public Weight getWeightFromUserOnDate(int userId, long date) {
        return mRepository.getWeightFromUserOnDate(userId, date);
    }

    public void insertWeight(Weight weight) {
        mRepository.insertWeight(weight);
    }

    public void updateWeight(Weight weight) {
        mRepository.updateWeight(weight);
    }

    public void deleteWeight(Weight weight) {
        mRepository.deleteWeight(weight);
    }

}

