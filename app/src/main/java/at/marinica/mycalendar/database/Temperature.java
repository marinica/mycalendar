package at.marinica.mycalendar.database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;
import static at.marinica.mycalendar.database.DateUtil.truncateTimestamp;

@Entity(tableName = "temperature", foreignKeys = @ForeignKey(entity = User.class,
        parentColumns = "id",
        childColumns = "userId",
        onDelete = CASCADE))
public class Temperature {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int userId;

    private long date;
    private double value;

    public Temperature(int userId, long date, double value) {
        this.userId = userId;
        this.date = truncateTimestamp(date);
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
