package at.marinica.mycalendar.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface NoteDao {

    @Insert
    void insert(Note note);

    @Query("DELETE FROM note")
    void deleteAll();

    @Delete
    void delete(Note... notes);

    @Update
    void update(Note... notes);

    @Query("SELECT * from note WHERE userId = :userId")
    LiveData<List<Note>> getNotesFromUser(int userId);

    @Query("SELECT * from note")
    LiveData<List<Note>> getAllNotes();

    @Query("SELECT * from note WHERE userId = :userId AND date = :date")
    Note getNoteFromDate(int userId, long date);

}
