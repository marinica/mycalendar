package at.marinica.mycalendar.database;

import java.util.Calendar;
import java.util.Date;

public final class DateUtil {

    public static long truncateTimestamp(long timestamp) {
        Date date = new Date(timestamp);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        date = cal.getTime();
        return date.getTime();
    }
}
