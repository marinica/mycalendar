package at.marinica.mycalendar.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface TemperatureDao {

    @Insert
    void insert(Temperature temperature);

    @Query("DELETE FROM temperature")
    void deleteAll();

    @Delete
    void delete(Temperature... temperatures);

    @Update
    void update(Temperature... temperatures);

    @Query("SELECT * from temperature WHERE userId = :userId AND date >= :startDate order by date")
    List<Temperature> getTemperaturesFromUserInTimeframe(int userId, long startDate);

    @Query("SELECT * from temperature WHERE userId = :userId AND date = :date")
    Temperature getTemperatureForExactDate(int userId, long date);

    @Query("SELECT * from temperature")
    LiveData<List<Temperature>> getAllTemperatures();

}
