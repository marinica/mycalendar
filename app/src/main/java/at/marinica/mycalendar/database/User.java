package at.marinica.mycalendar.database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "user")
public class User {

    private static final int DEFAULT_CYCLE_LENGTH = 28;
    private static final int DEFAULT_PERIOD_LENGTH = 5;

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    private String name;

    private int cycleLength;
    private int periodLength;

    public User(@NonNull String name) {
        this.name = name;
        cycleLength = DEFAULT_CYCLE_LENGTH;
        periodLength = DEFAULT_PERIOD_LENGTH;
    }

    @Ignore
    public User(int id, @NonNull String name) {
        this.id = id;
        this.name = name;
        cycleLength = DEFAULT_CYCLE_LENGTH;
        periodLength = DEFAULT_PERIOD_LENGTH;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    public int getCycleLength() {
        return cycleLength;
    }

    public void setCycleLength(int cycleLength) {
        this.cycleLength = cycleLength;
    }

    public int getPeriodLength() {
        return periodLength;
    }

    public void setPeriodLength(int periodLength) {
        this.periodLength = periodLength;
    }
}
