package at.marinica.mycalendar.database;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.support.annotation.Nullable;

import java.util.List;
import java.util.concurrent.ExecutionException;

import static at.marinica.mycalendar.database.DateUtil.truncateTimestamp;

public class MyCalendarRepository {

    private UserDao mUserDao;
    private LiveData<List<User>> mAllUsers;

    private PeriodDao mPeriodDao;
    private LiveData<List<Period>> mAllPeriods;


    private TemperatureDao mTemperatureDao;
    private LiveData<List<Temperature>> mAllTemperatures;

    private NoteDao mNoteDao;
    private LiveData<List<Note>> mAllNotes;

    private WeightDao mWeightDao;
    private LiveData<List<Weight>> mAllWeights;

    MyCalendarRepository(Application application) {
        MyCalendarRoomDatabase db = MyCalendarRoomDatabase.getDatabase(application);
        mUserDao = db.userDao();
        mAllUsers = mUserDao.getAllUsers();
        mPeriodDao = db.periodDao();
        mAllPeriods = mPeriodDao.getAllPeriods();
        mTemperatureDao = db.temperatureDao();
        mAllTemperatures = mTemperatureDao.getAllTemperatures();
        mNoteDao = db.noteDao();
        mAllNotes = mNoteDao.getAllNotes();
        mWeightDao = db.weightDao();
        mAllWeights = mWeightDao.getAllWeights();
    }

    LiveData<List<User>> getAllUsers() {
        return mAllUsers;
    }

    public User getUserById(int id) {
        try {
            return new QueryUserAsyncTask(mUserDao, id).execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Period getLastPeriodForUser(int userId) {
        try {
            return new QueryPeriodAsyncTask(mPeriodDao, userId).execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class QueryPeriodAsyncTask extends AsyncTask<Void, Void, Period> {

        private PeriodDao mAsyncTaskDao;
        private int userId;

        QueryPeriodAsyncTask(PeriodDao dao, int userId) {
            mAsyncTaskDao = dao;
            this.userId = userId;
        }

        @Override
        protected Period doInBackground(final Void... params) {
            return mAsyncTaskDao.getLastPeriodForUser(userId);
        }
    }

    public List<Period> getLastPeriodsForUser(int userId, int limit) {
        try {
            return new QueryPeriodsAsyncTask(mPeriodDao, userId, limit).execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class QueryPeriodsAsyncTask extends AsyncTask<Void, Void, List<Period>> {

        private PeriodDao mAsyncTaskDao;
        private int userId;
        private int limit;

        QueryPeriodsAsyncTask(PeriodDao dao, int userId, int limit) {
            mAsyncTaskDao = dao;
            this.userId = userId;
            this.limit = limit;
        }

        @Override
        protected List<Period> doInBackground(final Void... params) {
            return mAsyncTaskDao.getLastPeriodsForUser(userId, limit);
        }
    }

    private static class QueryUserAsyncTask extends AsyncTask<Void, Void, User> {

        private UserDao mAsyncTaskDao;
        private int userId;

        QueryUserAsyncTask(UserDao dao, int userId) {
            mAsyncTaskDao = dao;
            this.userId = userId;
        }

        @Override
        protected User doInBackground(final Void... params) {
            return mAsyncTaskDao.getUserById(userId);
        }
    }

    LiveData<List<Temperature>> getAllTemperatures() {
        return mAllTemperatures;
    }

    @Nullable
    List<Temperature> getTemperaturesFromUserInTimeframe(User user, long startDate) {
        try {
            return new QueryTemperatureTimeframeAsyncTask(mTemperatureDao, user, truncateTimestamp(startDate)).execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class QueryTemperatureTimeframeAsyncTask extends AsyncTask<Void, Void, List<Temperature>> {

        private TemperatureDao mAsyncTaskDao;
        private User user;
        private long startDate;

        QueryTemperatureTimeframeAsyncTask(TemperatureDao dao, User user, long startDate) {
            mAsyncTaskDao = dao;
            this.user = user;
            this.startDate = startDate;
        }

        @Override
        protected List<Temperature> doInBackground(final Void... params) {
           return mAsyncTaskDao.getTemperaturesFromUserInTimeframe(user.getId(), startDate);
        }
    }

    public Temperature getTemperatureFromUserOnDate(int userId, long date) {
        try {
            return new QueryTemperatureAsyncTask(mTemperatureDao, userId, date).execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class QueryTemperatureAsyncTask extends AsyncTask<Void, Void, Temperature> {

        private TemperatureDao mAsyncTaskDao;
        private int userId;
        private long date;

        QueryTemperatureAsyncTask(TemperatureDao dao, int userId, long date) {
            mAsyncTaskDao = dao;
            this.userId = userId;
            this.date = date;
        }

        @Override
        protected Temperature doInBackground(final Void... params) {
            return mAsyncTaskDao.getTemperatureForExactDate(userId, date);
        }
    }

    public void insertTemperature(Temperature temperature) {
        new InsertTemperatureAsyncTask(mTemperatureDao).execute(temperature);
    }

    private static class InsertTemperatureAsyncTask extends AsyncTask<Temperature, Void, Void> {

        private TemperatureDao mAsyncTaskDao;

        InsertTemperatureAsyncTask(TemperatureDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Temperature... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public void updateTemperature(Temperature temperature) {
        new UpdateTemperatureAsyncTask(mTemperatureDao).execute(temperature);
    }

    private static class UpdateTemperatureAsyncTask extends AsyncTask<Temperature, Void, Void> {

        private TemperatureDao mAsyncTaskDao;

        UpdateTemperatureAsyncTask(TemperatureDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Temperature... params) {
            mAsyncTaskDao.update(params[0]);
            return null;
        }
    }

    public void deleteTemperature(Temperature temperature) {
        new DeleteTemperatureAsyncTask(mTemperatureDao).execute(temperature);
    }

    private static class DeleteTemperatureAsyncTask extends AsyncTask<Temperature, Void, Void> {

        private TemperatureDao mAsyncTaskDao;

        DeleteTemperatureAsyncTask(TemperatureDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Temperature... params) {
            mAsyncTaskDao.delete(params[0]);
            return null;
        }
    }

    public void insertUser(User user) {
        new InsertUserAsyncTask(mUserDao).execute(user);
    }

    private static class InsertUserAsyncTask extends AsyncTask<User, Void, Void> {

        private UserDao mAsyncTaskDao;

        InsertUserAsyncTask(UserDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final User... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public void deleteUser(User user) {
        new DeleteUserAsyncTask(mUserDao).execute(user);
    }

    private static class DeleteUserAsyncTask extends AsyncTask<User, Void, Void> {

        private UserDao mAsyncTaskDao;

        DeleteUserAsyncTask(UserDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final User... params) {
            mAsyncTaskDao.deleteUsers(params);
            return null;
        }
    }

    public void updateUser(User user) {
        new UpdateUserAsyncTask(mUserDao).execute(user);
    }

    private static class UpdateUserAsyncTask extends AsyncTask<User, Void, Void> {

        private UserDao mAsyncTaskDao;

        UpdateUserAsyncTask(UserDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final User... params) {
            mAsyncTaskDao.update(params);
            return null;
        }
    }

    public void insertPeriod(Period period) {
        new InsertPeriodAsyncTask(mPeriodDao).execute(period);
    }

    private static class InsertPeriodAsyncTask extends AsyncTask<Period, Void, Void> {

        private PeriodDao mAsyncTaskDao;

        InsertPeriodAsyncTask(PeriodDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Period... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public void updatePeriod(Period period) {
        new UpdatePeriodAsyncTask(mPeriodDao).execute(period);
    }

    private static class UpdatePeriodAsyncTask extends AsyncTask<Period, Void, Void> {

        private PeriodDao mAsyncTaskDao;

        UpdatePeriodAsyncTask(PeriodDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Period... params) {
            mAsyncTaskDao.update(params);
            return null;
        }
    }

    public void deletePeriod(Period period) {
        new DeletePeriodAsyncTask(mPeriodDao).execute(period);
    }

    public LiveData<List<Period>> getAllPeriods() {
        return mAllPeriods;
    }


    public LiveData<List<Note>> getAllNotes() {
        return mAllNotes;
    }

    private static class DeletePeriodAsyncTask extends AsyncTask<Period, Void, Void> {

        private PeriodDao mAsyncTaskDao;

        DeletePeriodAsyncTask(PeriodDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Period... params) {
            mAsyncTaskDao.delete(params);
            return null;
        }
    }

    public void insertNote(Note note) {
        new InsertNoteAsyncTask(mNoteDao).execute(note);
    }

    private static class InsertNoteAsyncTask extends AsyncTask<Note, Void, Void> {

        private NoteDao mAsyncTaskDao;

        InsertNoteAsyncTask(NoteDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Note... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public void updateNote(Note note) {
        new UpdateNoteAsyncTask(mNoteDao).execute(note);
    }

    private static class UpdateNoteAsyncTask extends AsyncTask<Note, Void, Void> {

        private NoteDao mAsyncTaskDao;

        UpdateNoteAsyncTask(NoteDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Note... params) {
            mAsyncTaskDao.update(params);
            return null;
        }
    }

    public void deleteNote(Note note) {
        new DeleteNoteAsyncTask(mNoteDao).execute(note);
    }

    private static class DeleteNoteAsyncTask extends AsyncTask<Note, Void, Void> {

        private NoteDao mAsyncTaskDao;

        DeleteNoteAsyncTask(NoteDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Note... params) {
            mAsyncTaskDao.delete(params);
            return null;
        }
    }

    public Note getNoteByDate(int userId, long date) {
        try {
            return new QueryNoteAsyncTask(mNoteDao, userId, date).execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class QueryNoteAsyncTask extends AsyncTask<Void, Void, Note> {

        private NoteDao mAsyncTaskDao;
        private int userId;
        private long date;

        QueryNoteAsyncTask(NoteDao dao, int userId, long date) {
            mAsyncTaskDao = dao;
            this.userId = userId;
            this.date = date;
        }

        @Override
        protected Note doInBackground(final Void... params) {
            return mAsyncTaskDao.getNoteFromDate(userId, date);
        }
    }

    LiveData<List<Weight>> getAllWeights() {
        return mAllWeights;
    }

    @Nullable
    List<Weight> getWeightsFromUserInTimeframe(User user, long startDate) {
        try {
            return new QueryWeightTimeframeAsyncTask(mWeightDao, user, truncateTimestamp(startDate)).execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class QueryWeightTimeframeAsyncTask extends AsyncTask<Void, Void, List<Weight>> {

        private WeightDao mAsyncTaskDao;
        private User user;
        private long startDate;

        QueryWeightTimeframeAsyncTask(WeightDao dao, User user, long startDate) {
            mAsyncTaskDao = dao;
            this.user = user;
            this.startDate = startDate;
        }

        @Override
        protected List<Weight> doInBackground(final Void... params) {
            return mAsyncTaskDao.getWeightsFromUserInTimeframe(user.getId(), startDate);
        }
    }

    public Weight getWeightFromUserOnDate(int userId, long date) {
        try {
            return new QueryWeightAsyncTask(mWeightDao, userId, date).execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static class QueryWeightAsyncTask extends AsyncTask<Void, Void, Weight> {

        private WeightDao mAsyncTaskDao;
        private int userId;
        private long date;

        QueryWeightAsyncTask(WeightDao dao, int userId, long date) {
            mAsyncTaskDao = dao;
            this.userId = userId;
            this.date = date;
        }

        @Override
        protected Weight doInBackground(final Void... params) {
            return mAsyncTaskDao.getWeightForExactDate(userId, date);
        }
    }

    public void insertWeight(Weight weight) {
        new InsertWeightAsyncTask(mWeightDao).execute(weight);
    }

    private static class InsertWeightAsyncTask extends AsyncTask<Weight, Void, Void> {

        private WeightDao mAsyncTaskDao;

        InsertWeightAsyncTask(WeightDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Weight... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    public void updateWeight(Weight weight) {
        new UpdateWeightAsyncTask(mWeightDao).execute(weight);
    }

    private static class UpdateWeightAsyncTask extends AsyncTask<Weight, Void, Void> {

        private WeightDao mAsyncTaskDao;

        UpdateWeightAsyncTask(WeightDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Weight... params) {
            mAsyncTaskDao.update(params[0]);
            return null;
        }
    }

    public void deleteWeight(Weight weight) {
        new DeleteWeightAsyncTask(mWeightDao).execute(weight);
    }

    private static class DeleteWeightAsyncTask extends AsyncTask<Weight, Void, Void> {

        private WeightDao mAsyncTaskDao;

        DeleteWeightAsyncTask(WeightDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Weight... params) {
            mAsyncTaskDao.delete(params[0]);
            return null;
        }
    }

}