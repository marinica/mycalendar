package at.marinica.mycalendar;

import android.content.Context;

import com.jjoe64.graphview.DefaultLabelFormatter;

import java.text.DateFormat;
import java.util.Calendar;

public class DateAndYLabelFormatter extends DefaultLabelFormatter {
    /**
     * the date format that will convert
     * the unix timestamp to string
     */
    protected final DateFormat mDateFormat;

    /**
     * calendar to avoid creating new date objects
     */
    protected final Calendar mCalendar;
    protected final String yAxis;

    /**
     * create the formatter with the Android default date format to convert
     * the x-values.
     *
     * @param context the application context
     * @param yAxis
     */
    public DateAndYLabelFormatter(Context context, String yAxis) {
        mDateFormat = android.text.format.DateFormat.getDateFormat(context);
        this.yAxis = yAxis;
        mCalendar = Calendar.getInstance();
    }

    /**
     * create the formatter with your own custom
     * date format to convert the x-values.
     *  @param context the application context
     * @param dateFormat custom date format
     * @param yAxis
     */
    public DateAndYLabelFormatter(Context context, DateFormat dateFormat, String yAxis) {
        mDateFormat = dateFormat;
        this.yAxis = yAxis;
        mCalendar = Calendar.getInstance();
    }

    /**
     * formats the x-values as date string.
     *
     * @param value raw value
     * @param isValueX true if it's a x value, otherwise false
     * @return value converted to string
     */
    @Override
    public String formatLabel(double value, boolean isValueX) {
        if (isValueX) {
            // format as date
            mCalendar.setTimeInMillis((long) value);
            return mDateFormat.format(mCalendar.getTimeInMillis());
        } else {
            return super.formatLabel(value, isValueX) + " " + yAxis;
        }
    }
}
