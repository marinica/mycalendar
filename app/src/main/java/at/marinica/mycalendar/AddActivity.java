package at.marinica.mycalendar;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import at.marinica.mycalendar.database.MyCalendarViewModel;
import at.marinica.mycalendar.database.User;

public class AddActivity extends AppCompatActivity {

    private static final String TAG = "AddActivity";

    private MyCalendarViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MyCalendarViewModel.class);
        setContentView(R.layout.activity_add_);
        final Button save = findViewById(R.id.save);
        final TextView userName = findViewById(R.id.userName);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            String userNameStr = String.valueOf(userName.getText());
            if (userNameStr == null || userNameStr.length() <= 0) {
                Toast.makeText(AddActivity.this, "Please enter a valid user name", Toast.LENGTH_SHORT).show();
            }
            else {
                User user = new User(userNameStr);
                mViewModel.insertUser(user);
                Toast.makeText(AddActivity.this, "User " + userNameStr + " saved", Toast.LENGTH_SHORT).show();
                finish();
            }
            }
        });
    }
}
