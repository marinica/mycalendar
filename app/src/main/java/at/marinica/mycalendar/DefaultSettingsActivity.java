package at.marinica.mycalendar;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import at.marinica.mycalendar.database.MyCalendarViewModel;
import at.marinica.mycalendar.database.User;

public class DefaultSettingsActivity extends AppCompatActivity {

    private static final String TAG = "DefaultSettingsActivity";

    private MyCalendarViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MyCalendarViewModel.class);
        Intent intent = getIntent();
        int userId = intent.getIntExtra("userId", 0);
        final User user = mViewModel.getUserById(userId);
        Log.d(TAG, user.getName() + " in settings activity");
        setContentView(R.layout.default_settings);
        final TextView cycleLength = findViewById(R.id.cycleLength);
        cycleLength.setText(String.valueOf(user.getCycleLength()));
        final TextView menstruationLength = findViewById(R.id.menstruationLength);
        menstruationLength.setText(String.valueOf(user.getPeriodLength()));

        final Button save = findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cycleLengthStr = String.valueOf(cycleLength.getText());
                if (cycleLengthStr != null && cycleLengthStr.length() > 0) {
                    user.setCycleLength(Integer.parseInt(cycleLengthStr));
                }
                String periodLengthStr = String.valueOf(menstruationLength.getText());
                if (periodLengthStr != null && periodLengthStr.length() > 0) {
                    user.setPeriodLength(Integer.parseInt(periodLengthStr));
                }
                mViewModel.updateUser(user);
                Toast.makeText(DefaultSettingsActivity.this, "Settings saved", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

