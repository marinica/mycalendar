package at.marinica.mycalendar;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import at.marinica.mycalendar.database.MyCalendarViewModel;
import at.marinica.mycalendar.database.Note;
import at.marinica.mycalendar.database.NoteDao;

public class NoteActivity extends AppCompatActivity {

    private MyCalendarViewModel mViewModel;
    private int userId;
    private String dateString;
    private long dateLong;
    private Note currentNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        mViewModel = ViewModelProviders.of(this).get(MyCalendarViewModel.class);

        Intent intent = getIntent();

        userId = intent.getIntExtra("userId", 0);
        dateString = intent.getStringExtra("selectedDate");
        //System.err.println(dateString);
        dateLong = intent.getLongExtra("timestamp", 0);

        updateNoteText(userId, dateString, dateLong);

        Button saveBtn = (Button) findViewById(R.id.saveNoteButton);
        saveBtn.setOnClickListener((e) -> {
            final EditText noteTextView = (EditText)findViewById(R.id.note);
            String text = noteTextView.getText().toString();
            if( currentNote != null ) {
                if(!currentNote.getText().equals(text) && !text.equals("")) {
                    currentNote.setText(text);
                    mViewModel.updateNote(currentNote);
                } else if (text.equals("")){
                    mViewModel.deleteNote(currentNote);
                }
            } else {
                currentNote = new Note(userId, dateLong, text);
                mViewModel.insertNote(currentNote);
            }

            this.finish();
        });
    }

    private void updateNoteText(int userId, String dateString, long dateLong) {

        final EditText note = (EditText)findViewById(R.id.note);
        final TextView selectedDate = (TextView)findViewById(R.id.selectedDate);

        selectedDate.setText(dateString);

        Note savedNote = mViewModel.getNoteByDate(userId, dateLong);
        if(savedNote != null){
            currentNote = savedNote;
            String noteText = savedNote.getText();
            note.setText(noteText);
            note.invalidate();
        }

        //events.add(new EventDay(clickedDayCalendar, R.drawable.note));
        //calendarView.setEvents(events);
}

    /*@Override
    protected void onDestroy() {
        super.onDestroy();
        System.err.println("Destroy");
        //updateNoteText(userId, dateString, dateLong);
    }*/
}
