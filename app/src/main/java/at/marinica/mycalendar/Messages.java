package at.marinica.mycalendar;

public final class Messages {

    public static final int STORE_TEMPERATURE = 0;
    public static final int STORE_WEIGHT = 1;

    public static final String UserId = "userId";
    public static final String Suggestion = "suggestion";
    public static final String StoreType = "storeType";

}
