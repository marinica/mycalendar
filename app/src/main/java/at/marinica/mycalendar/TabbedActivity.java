package at.marinica.mycalendar;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import at.marinica.mycalendar.database.MyCalendarViewModel;
import at.marinica.mycalendar.database.Temperature;
import at.marinica.mycalendar.database.User;
import at.marinica.mycalendar.ui.SpinnerAdapter;
import at.marinica.mycalendar.ui.UserSelectionChangedListener;


public class TabbedActivity extends AppCompatActivity {

    private static final String TAG = "TabbedActivity";

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private Spinner spinner;

    private MyCalendarViewModel mViewModel;
    private User currentUser;

    private TemperatureFragment temperatureFragment;

    private List<UserSelectionChangedListener> userSelectionChangedListeners;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabbed);

        mViewModel = ViewModelProviders.of(this).get(MyCalendarViewModel.class);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        userSelectionChangedListeners = new LinkedList<>();

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        temperatureFragment = new TemperatureFragment();


        final FloatingActionButton fab_period = findViewById(R.id.fab_period);
        fab_period.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popup = new PopupMenu(TabbedActivity.this, fab_period);
                popup.getMenuInflater().inflate(R.menu.menu_btn, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        int id = item.getItemId();
                        if(id == R.id.start){
                            if (currentUser != null) {
                                Intent intent = new Intent(TabbedActivity.this, EnterStartPeriodActivity.class);
                                intent.putExtra(Messages.UserId, currentUser.getId());
                                startActivity(intent);
                            }
                        } else if (id == R.id.end){
                            if (currentUser != null) {
                                Intent intent = new Intent(TabbedActivity.this, EnterEndPeriodActivity.class);
                                intent.putExtra(Messages.UserId, currentUser.getId());
                                startActivity(intent);
                            }
                        }
                        return true;
                    }
                });
                popup.show();//showing popup menu
            }
        });

        final FloatingActionButton fab_temp = findViewById(R.id.fab_temp);
        fab_temp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentUser != null) {
                    Intent intent = new Intent(TabbedActivity.this, OcrCaptureActivity.class);
                    intent.putExtra(Messages.UserId, currentUser.getId());
                    intent.putExtra(Messages.StoreType, Messages.STORE_TEMPERATURE);
                    startActivity(intent);
                }
            }
        });

        final FloatingActionButton fab_weight = findViewById(R.id.fab_weight);
        fab_weight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentUser != null) {
                    Intent intent = new Intent(TabbedActivity.this, OcrCaptureActivity.class);
                    intent.putExtra(Messages.UserId, currentUser.getId());
                    intent.putExtra(Messages.StoreType, Messages.STORE_WEIGHT);
                    startActivity(intent);
                }
            }
        });

        spinner = findViewById(R.id.profiles);
        LiveData<List<User>> allUsers = mViewModel.getAllUsers();
        final SpinnerAdapter adapter = new SpinnerAdapter(this, R.layout.spinner_item, allUsers.getValue() == null ? new ArrayList<>() : new ArrayList<>(allUsers.getValue()));
        allUsers.observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(@Nullable final List<User> users) {
                if (users != null) {
                    adapter.clear();
                    adapter.addAll(users);
                    for (User user : users) {
                        if (currentUser == null) {
                            setUser(user);
                        }
                        List<Temperature> temperaturesFromUserInTimeframe = mViewModel.getTemperaturesFromUserInTimeframe(user, System.currentTimeMillis());
                        if (temperaturesFromUserInTimeframe != null) {
                            for (Temperature t : temperaturesFromUserInTimeframe) {
                                Log.d(TAG, user.getName() + ": " + t.getDate() + ", " + t.getValue());
                            }
                        }
                    }
                }
                if (users == null || users.size() == 0) {
                    showCreateUserDialog();
                }
            }
        });
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                User user = (User) spinner.getSelectedItem();
                Log.d(TAG, user.getName() + " selected");
                setUser(user);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void addUserSelectionChangedListener(UserSelectionChangedListener listener) {
        userSelectionChangedListeners.add(listener);
    }

    private void notifyUserSelectionChangedListeners(@Nullable User user) {
        for (UserSelectionChangedListener l : userSelectionChangedListeners) {
            try {
                l.userSelectionChanged(user);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setUser(User user) {
        if (currentUser == null && user == null) {
            // no change
            return;
        }
        else if (currentUser != null && user != null && currentUser.getId() == user.getId()) {
            // no change
            return;
        }
        currentUser = user;
        notifyUserSelectionChangedListeners(user);
    }

    public User getCurrentUser(){
        return currentUser;
    }

    private void showCreateUserDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("No users");
        alertDialog.setMessage("Please create a user to proceed");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(TabbedActivity.this, AddActivity.class);
                        startActivity(intent);
                    }
                });
        alertDialog.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tabbed, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, DefaultSettingsActivity.class);
            intent.putExtra(Messages.UserId, currentUser.getId());
            startActivity(intent);
            return true;
        }
        else if (id == R.id.add) {
            Intent intent = new Intent(this, AddActivity.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.delete){
            if (currentUser != null) {
                AlertDialog alertDialog = new AlertDialog.Builder(TabbedActivity.this).create();
                alertDialog.setTitle("Delete User");
                alertDialog.setMessage("Are you sure you want to delete the user \""+ currentUser.getName() +"\"?");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                mViewModel.deleteUser(currentUser);
                                dialog.dismiss();
                                Toast.makeText(TabbedActivity.this, "User \""+ currentUser.getName() +"\" was successfully deleted", Toast.LENGTH_SHORT).show();
                                setUser((User) spinner.getSelectedItem());
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.show();
            }
        }

        return super.onOptionsItemSelected(item);
    }


     /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    return new CalendarFragment();
                case 1:
                    return temperatureFragment;
                case 2:
                    return new StatisticFragment();
                case 3:
                    return new WeightFragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 4;
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        
    }
}
