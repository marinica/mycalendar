package at.marinica.mycalendar;

import android.app.DatePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import at.marinica.mycalendar.database.DateUtil;
import at.marinica.mycalendar.database.MyCalendarViewModel;
import at.marinica.mycalendar.database.Temperature;

public class EnterTempActivity extends AppCompatActivity {

    private static final String TAG = "EnterTempActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_temp);
        MyCalendarViewModel mViewModel = ViewModelProviders.of(this).get(MyCalendarViewModel.class);
        Intent intent = getIntent();
        int userId = intent.getIntExtra(Messages.UserId, 0);

        TextView dateView = (TextView) findViewById(R.id.date);

        final Calendar myCalendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");//formating according to my need
        Date today = myCalendar.getTime();//getting date
        String date = formatter.format(today);

        dateView.setText(date);

        DatePickerDialog.OnDateSetListener datePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                dateView.setText(formatter.format(myCalendar.getTime()));
            }

        };

        dateView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EnterTempActivity.this, datePicker, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        double suggestTemperature = intent.getDoubleExtra(Messages.Suggestion, 0.0);
        TextView temperature = findViewById(R.id.temperature);
        if (suggestTemperature > 0) {
            temperature.setText(String.valueOf(suggestTemperature));
        }

        Button saveBtn = (Button) findViewById(R.id.save);
        saveBtn.setOnClickListener((e) -> {
            //save temperature
            try {
                Date parse = formatter.parse(String.valueOf(dateView.getText()));
                double parseTemp = Double.parseDouble(String.valueOf(temperature.getText()));
                long timestamp = DateUtil.truncateTimestamp(parse.getTime());
                Temperature temperatureFromUserOnDate = mViewModel.getTemperatureFromUserOnDate(userId, timestamp);
                if (temperatureFromUserOnDate == null) {
                    temperatureFromUserOnDate = new Temperature(userId, timestamp, parseTemp);
                    mViewModel.insertTemperature(temperatureFromUserOnDate);
                    Log.d(TAG, String.format("Inserted new temperature: id %d, userId %d, date %d, value %f", temperatureFromUserOnDate.getId(), temperatureFromUserOnDate.getUserId(), temperatureFromUserOnDate.getDate(), temperatureFromUserOnDate.getValue()));
                }
                else {
                    temperatureFromUserOnDate.setValue(parseTemp);
                    mViewModel.updateTemperature(temperatureFromUserOnDate);
                    Log.d(TAG, String.format("Updated temperature: id %d, userId %d, date %d, value %f", temperatureFromUserOnDate.getId(), temperatureFromUserOnDate.getUserId(), temperatureFromUserOnDate.getDate(), temperatureFromUserOnDate.getValue()));
                }
                finish();
            } catch (ParseException ex) {
                Toast.makeText(EnterTempActivity.this, "Invalid date entered", Toast.LENGTH_SHORT).show();
            } catch (NumberFormatException ex) {
                Toast.makeText(EnterTempActivity.this, "Invalid temperature entered", Toast.LENGTH_SHORT).show();
            }
            });

    }
}
