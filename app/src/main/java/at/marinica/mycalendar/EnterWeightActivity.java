package at.marinica.mycalendar;

import android.app.DatePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import at.marinica.mycalendar.database.DateUtil;
import at.marinica.mycalendar.database.MyCalendarViewModel;
import at.marinica.mycalendar.database.Weight;

public class EnterWeightActivity extends AppCompatActivity {

    private static final String TAG = "EnterWeightActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_weight);
        MyCalendarViewModel mViewModel = ViewModelProviders.of(this).get(MyCalendarViewModel.class);
        Intent intent = getIntent();
        int userId = intent.getIntExtra(Messages.UserId, 0);

        TextView dateView = (TextView) findViewById(R.id.date);

        final Calendar myCalendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");//formating according to my need
        Date today = myCalendar.getTime();//getting date
        String date = formatter.format(today);

        dateView.setText(date);

        DatePickerDialog.OnDateSetListener datePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                dateView.setText(formatter.format(myCalendar.getTime()));
            }

        };

        dateView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EnterWeightActivity.this, datePicker, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        double suggestWeight = intent.getDoubleExtra(Messages.Suggestion, 0.0);
        TextView weight = findViewById(R.id.weight);
        if (suggestWeight > 0) {
            weight.setText(String.valueOf(suggestWeight));
        }

        Button saveBtn = (Button) findViewById(R.id.save);
        saveBtn.setOnClickListener((e) -> {
            //save date and weight
            try {
                Date parse = formatter.parse(String.valueOf(dateView.getText()));
                double parseTemp = Double.parseDouble(String.valueOf(weight.getText()));
                long timestamp = DateUtil.truncateTimestamp(parse.getTime());
                Weight weightFromUserOnDate = mViewModel.getWeightFromUserOnDate(userId, timestamp);
                if (weightFromUserOnDate == null) {
                    weightFromUserOnDate = new Weight(userId, timestamp, parseTemp);
                    mViewModel.insertWeight(weightFromUserOnDate);
                    Log.d(TAG, String.format("Inserted new weight: id %d, userId %d, date %d, value %f", weightFromUserOnDate.getId(), weightFromUserOnDate.getUserId(), weightFromUserOnDate.getDate(), weightFromUserOnDate.getValue()));
                }
                else {
                    weightFromUserOnDate.setValue(parseTemp);
                    mViewModel.updateWeight(weightFromUserOnDate);
                    Log.d(TAG, String.format("Updated weight: id %d, userId %d, date %d, value %f", weightFromUserOnDate.getId(), weightFromUserOnDate.getUserId(), weightFromUserOnDate.getDate(), weightFromUserOnDate.getValue()));
                }
                finish();
            } catch (ParseException ex) {
                Toast.makeText(EnterWeightActivity.this, "Invalid date entered", Toast.LENGTH_SHORT).show();
            } catch (NumberFormatException ex) {
                Toast.makeText(EnterWeightActivity.this, "Invalid weight entered", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
