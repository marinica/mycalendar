package at.marinica.mycalendar;

import android.app.DatePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import at.marinica.mycalendar.database.DateUtil;
import at.marinica.mycalendar.database.MyCalendarViewModel;
import at.marinica.mycalendar.database.Period;

public class EnterStartPeriodActivity extends AppCompatActivity {

    private static final String TAG = "EnterStartPeriodActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_start_period);
        MyCalendarViewModel mViewModel = ViewModelProviders.of(this).get(MyCalendarViewModel.class);
        Intent intent = getIntent();
        int userId = intent.getIntExtra("userId", 0);

        TextView dateView = (TextView) findViewById(R.id.date);

        final Calendar myCalendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");//formating according to my need
        Date today = myCalendar.getTime();//getting date
        String date = formatter.format(today);

        dateView.setText(date);

        DatePickerDialog.OnDateSetListener datePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                dateView.setText(formatter.format(myCalendar.getTime()));
            }

        };

        dateView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EnterStartPeriodActivity.this, datePicker, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        Button saveBtn = (Button) findViewById(R.id.save);
        saveBtn.setOnClickListener((e) -> {
            //save start period date
            try {
                Date parse = formatter.parse(String.valueOf(dateView.getText()));
                Period lastPeriodForUser = mViewModel.getLastPeriodForUser(userId);
                if (lastPeriodForUser == null || lastPeriodForUser.getEndDate() > 0) {
                    Period period = new Period(userId, DateUtil.truncateTimestamp(parse.getTime()));
                    mViewModel.insertPeriod(period);
                    Log.d(TAG, String.format("Inserted new period: id %d, userId %d, startDate %d, endDate %d", period.getId(), period.getUserId(), period.getStartDate(), period.getEndDate()));
                }
                else {
                    lastPeriodForUser.setStartDate(DateUtil.truncateTimestamp(parse.getTime()));
                    mViewModel.updatePeriod(lastPeriodForUser);
                    Log.d(TAG, String.format("Updated last period: id %d, userId %d, startDate %d, endDate %d", lastPeriodForUser.getId(), lastPeriodForUser.getUserId(), lastPeriodForUser.getStartDate(), lastPeriodForUser.getEndDate()));
                }
                finish();
            } catch (ParseException ex) {
                Toast.makeText(EnterStartPeriodActivity.this, "Invalid date entered", Toast.LENGTH_SHORT).show();
            }
        });


    }


}
