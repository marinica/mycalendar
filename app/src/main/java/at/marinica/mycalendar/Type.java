package at.marinica.mycalendar;

public enum Type {
    PERIOD, NOTE, OVULATION, TEMPERATURE, WEIGHT
}
