package at.marinica.mycalendar.ui;

import android.support.annotation.Nullable;

import at.marinica.mycalendar.database.User;

public interface UserSelectionChangedListener {

    void userSelectionChanged(@Nullable User user);

}
