package at.marinica.mycalendar.ui;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.Collection;
import java.util.List;

import at.marinica.mycalendar.database.User;

public class SpinnerAdapter extends ArrayAdapter<User> {

    private static final String TAG = "SpinnerAdapter";
    private List<User> array;
    private static final int[] colorMap = new int[] {Color.parseColor("#b2eaa7"), Color.parseColor("#a7b2ea"), Color.parseColor("#eae0a7")};

    public SpinnerAdapter(@NonNull Context context, int resource, @NonNull List<User> objects) {
        super(context, resource, objects);
        this.array = objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View v = super.getView(position, convertView, parent);

        ((TextView) v).setGravity(Gravity.CENTER);
        TextView tv = v.findViewById(android.R.id.text1);
        tv.setText(array.get(position).getName());
        int color = colorMap[array.get(position).getId() % colorMap.length];
        v.setBackgroundColor(color);
        ((TextView) v).setTextColor(Color.WHITE);

        return v;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        View v = super.getDropDownView(position, convertView, parent);
        ((TextView) v).setGravity(Gravity.CENTER);
        TextView tv = v.findViewById(android.R.id.text1);
        tv.setText(array.get(position).getName());
        int color = colorMap[array.get(position).getId() % colorMap.length];
        v.setBackgroundColor(color);
        ((TextView) v).setTextColor(Color.WHITE);
        //Log.d(TAG, array.get(position).getName() + " " + array.get(position).getId());
        return v;
    }

    @Override
    public void addAll(@NonNull Collection<? extends User> collection) {
        super.addAll(collection);
        array.clear();
        array.addAll(collection);
    }

}
