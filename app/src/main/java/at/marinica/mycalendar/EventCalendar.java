package at.marinica.mycalendar;

import java.util.Date;

public class EventCalendar {

    private int drawable;
    private Type type;
    private long day;

    public EventCalendar(int drawable, Type type, long day) {
        this.drawable = drawable;
        this.type = type;
        this.day = day;
    }

    public long getDay() {
        return day;
    }

    public void setDay(long day) {
        this.day = day;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }
}
