/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package at.marinica.mycalendar;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.SparseArray;

import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;

import java.util.concurrent.atomic.AtomicBoolean;

import at.marinica.mycalendar.database.DateUtil;
import at.marinica.mycalendar.database.MyCalendarViewModel;
import at.marinica.mycalendar.database.Temperature;
import at.marinica.mycalendar.database.Weight;
import at.marinica.mycalendar.ui.camera.GraphicOverlay;

/**
 * A very simple Processor which gets detected TextBlocks and adds them to the overlay
 * as OcrGraphics.
 */
public class OcrDetectorProcessor implements Detector.Processor<TextBlock> {

    private static final String TAG = "OcrDetectorProcessor";

    private GraphicOverlay<OcrGraphic> graphicOverlay;
    private OcrCaptureActivity activity;
    private MyCalendarViewModel mViewModel;
    private int storeType;

    OcrDetectorProcessor(GraphicOverlay<OcrGraphic> ocrGraphicOverlay, OcrCaptureActivity activity, MyCalendarViewModel myCalendarViewModel, int storeType) {
        graphicOverlay = ocrGraphicOverlay;
        this.activity = activity;
        detectedNumber = new AtomicBoolean();
        detectedNumber.set(false);
        mViewModel = myCalendarViewModel;
        this.storeType = storeType;
    }

    private volatile AtomicBoolean detectedNumber;

    /**
     * Called by the detector to deliver detection results.
     * If your application called for it, this could be a place to check for
     * equivalent detections by tracking TextBlocks that are similar in location and content from
     * previous frames, or reduce noise by eliminating TextBlocks that have not persisted through
     * multiple detections.
     */
    @Override
    public void receiveDetections(Detector.Detections<TextBlock> detections) {
        graphicOverlay.clear();
        SparseArray<TextBlock> items = detections.getDetectedItems();
        for (int i = 0; i < items.size(); ++i) {
            TextBlock item = items.valueAt(i);
            if (item != null && item.getValue() != null) {
                String value = item.getValue();
                Log.d("OcrDetectorProcessor", "Text detected! " + value);
                OcrGraphic graphic = new OcrGraphic(graphicOverlay, item);
                graphicOverlay.add(graphic);
                String replaced = value.replace(',', '.');
                replaced = replaced.replace('o', '0');
                replaced = replaced.replace('O', '0');
                replaced = replaced.replaceAll("[^.0123456789]","");
                if (!detectedNumber.get() && !replaced.isEmpty()) {
                    StringBuilder builder = new StringBuilder(replaced);
                    int index = replaced.indexOf('1');
                    while(index >= 0) {
                        if (Math.random() > 0.5) {
                            builder.setCharAt(index, 's');
                        }
                        index = replaced.indexOf('1', index+1);
                    }
                    index = replaced.indexOf('7');
                    while(index >= 0) {
                        if (Math.random() > 0.5) {
                            builder.setCharAt(index, 'o');
                        }
                        index = replaced.indexOf('7', index+1);
                    }
                    replaced = builder.toString();
                    replaced = replaced.replace('s', '7');
                    replaced = replaced.replace('o', '1');
                    try {
                        double numericValue = Double.parseDouble(replaced);
                        if (numericValue >= 30.0) {
                            while (numericValue > 300.0) {
                                numericValue = numericValue / 10.0;
                            }
                            final double finalValue = numericValue;
                            detectedNumber.set(true);
                            Vibrator v = (Vibrator) activity.getSystemService(Context.VIBRATOR_SERVICE);
                            // Vibrate for 500 milliseconds
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                            } else {
                                //deprecated in API 26
                                v.vibrate(500);
                            }
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
                                    alertDialog.setTitle("Number detected");
                                    alertDialog.setMessage(String.valueOf(finalValue));
                                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    long timestamp = DateUtil.truncateTimestamp(System.currentTimeMillis());
                                                    if (storeType == Messages.STORE_TEMPERATURE) {
                                                        Temperature temperatureFromUserOnDate = mViewModel.getTemperatureFromUserOnDate(activity.getUserId(), timestamp);
                                                        if (temperatureFromUserOnDate == null) {
                                                            temperatureFromUserOnDate = new Temperature(activity.getUserId(), timestamp, finalValue);
                                                            mViewModel.insertTemperature(temperatureFromUserOnDate);
                                                            Log.d(TAG, String.format("Inserted new temperature: id %d, userId %d, date %d, value %f", temperatureFromUserOnDate.getId(), temperatureFromUserOnDate.getUserId(), temperatureFromUserOnDate.getDate(), temperatureFromUserOnDate.getValue()));
                                                        }
                                                        else {
                                                            temperatureFromUserOnDate.setValue(finalValue);
                                                            mViewModel.updateTemperature(temperatureFromUserOnDate);
                                                            Log.d(TAG, String.format("Updated temperature: id %d, userId %d, date %d, value %f", temperatureFromUserOnDate.getId(), temperatureFromUserOnDate.getUserId(), temperatureFromUserOnDate.getDate(), temperatureFromUserOnDate.getValue()));
                                                        }
                                                    }
                                                    else {
                                                        Weight weightFromUserOnDate = mViewModel.getWeightFromUserOnDate(activity.getUserId(), timestamp);
                                                        if (weightFromUserOnDate == null) {
                                                            weightFromUserOnDate = new Weight(activity.getUserId(), timestamp, finalValue);
                                                            mViewModel.insertWeight(weightFromUserOnDate);
                                                            Log.d(TAG, String.format("Inserted new weight: id %d, userId %d, date %d, value %f", weightFromUserOnDate.getId(), weightFromUserOnDate.getUserId(), weightFromUserOnDate.getDate(), weightFromUserOnDate.getValue()));
                                                        }
                                                        else {
                                                            weightFromUserOnDate.setValue(finalValue);
                                                            mViewModel.updateWeight(weightFromUserOnDate);
                                                            Log.d(TAG, String.format("Updated weight: id %d, userId %d, date %d, value %f", weightFromUserOnDate.getId(), weightFromUserOnDate.getUserId(), weightFromUserOnDate.getDate(), weightFromUserOnDate.getValue()));
                                                        }
                                                    }

                                                    activity.finish();
                                                }
                                            });
                                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Try Again",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    detectedNumber.set(false);
                                                    dialog.dismiss();
                                                }
                                            });
                                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Enter Manually",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Intent intent;
                                                    if (storeType == Messages.STORE_TEMPERATURE) {
                                                        intent = new Intent(activity, EnterTempActivity.class);
                                                    }
                                                    else {
                                                        intent = new Intent(activity, EnterWeightActivity.class);
                                                    }
                                                    intent.putExtra(Messages.UserId, activity.getUserId());
                                                    intent.putExtra(Messages.Suggestion, finalValue);
                                                    activity.startActivity(intent);
                                                    activity.finish();
                                                }
                                            });
                                    alertDialog.show();
                                }
                            });
                        }
                    }
                    catch (NumberFormatException e) {

                    }
                }
            }
        }
    }

    /**
     * Frees the resources associated with this detection processor.
     */
    @Override
    public void release() {
        graphicOverlay.clear();
    }
}
