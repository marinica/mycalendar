package at.marinica.mycalendar;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import at.marinica.mycalendar.database.DateUtil;
import at.marinica.mycalendar.database.MyCalendarViewModel;
import at.marinica.mycalendar.database.Note;
import at.marinica.mycalendar.database.Period;
import at.marinica.mycalendar.database.Temperature;
import at.marinica.mycalendar.database.User;
import at.marinica.mycalendar.database.Weight;


public class CalendarFragment extends Fragment {

    private static final String TAG = "CalendarFragment";

    //protected List<EventCalendar> eventsForCalendar = new ArrayList<>();
    protected List<Period> periodEvents = new ArrayList<>();
    protected List<Note> noteEvents = new ArrayList<>();
    protected List<Temperature> tempEvents = new ArrayList<>();
    protected List<Weight> weightEvents = new ArrayList<>();
    protected Map<Long, Set<Type>> currentEvents = new HashMap<>();

    TabbedActivity activity;
    private CalendarView calendarView;
    private MyCalendarViewModel mViewModel;

    protected void updateEvents(User currentUser){
        currentEvents.clear();
        if (currentUser == null) {
            return;
        }
        Period lastPeriod = mViewModel.getLastPeriodForUser(activity.getCurrentUser().getId());
        if (lastPeriod != null) {
            long ovulation = calculateOvulation(lastPeriod.getStartDate(), activity.getCurrentUser().getCycleLength());
            Set<Type> type = currentEvents.computeIfAbsent(ovulation, k-> new HashSet<>());
            type.add(Type.OVULATION);
        }


        for(Note n : noteEvents) {
            if (n.getUserId() == currentUser.getId()) {
                Set<Type> types = currentEvents.computeIfAbsent(n.getDate(), k -> new HashSet<>());
                types.add(Type.NOTE);
            }
        }

        for(Period p : periodEvents) {
            Log.d(TAG, String.format("Period found: user %d, startDate %d, endDate %d", p.getUserId(), p.getStartDate(), p.getEndDate()));
            if (p.getUserId() == currentUser.getId()) {
                long endDate = p.getEndDate();
                if (endDate == 0) {
                    endDate = DateUtil.truncateTimestamp(System.currentTimeMillis());
                }
                for (long d = p.getStartDate(); d <= endDate; d += 24 * 60 * 60 * 1_000) {
                    Log.d(TAG, "Timestamp: " + d);
                    Set<Type> types = currentEvents.computeIfAbsent(d, k -> new HashSet<>());
                    types.add(Type.PERIOD);
                }
            }
        }

        for(Weight w : weightEvents) {
            if (w.getUserId() == currentUser.getId()) {
                Set<Type> types = currentEvents.computeIfAbsent(w.getDate(), k -> new HashSet<>());
                types.add(Type.WEIGHT);
            }
        }

        for(Temperature t : tempEvents) {
            if (t.getUserId() == currentUser.getId()) {
                Set<Type> types = currentEvents.computeIfAbsent(t.getDate(), k -> new HashSet<>());
                types.add(Type.TEMPERATURE);
            }
        }
    }

    protected List<EventDay> convertEvents(){
        List<EventDay> events = new ArrayList<>();
        Set<Long> eventDays = currentEvents.keySet();


        for (Long day : eventDays){
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(day);

            if (currentEvents.get(day).size() == 1){
                Type type = currentEvents.get(day).iterator().next();
                if (type == Type.NOTE){
                    events.add(new EventDay(c, R.drawable.note));
                }
                if (type == Type.PERIOD){
                    events.add(new EventDay(c, R.drawable.period_red));
                }
                if (type == Type.WEIGHT){
                    events.add(new EventDay(c, R.drawable.weight_white));
                }
                if (type == Type.TEMPERATURE){
                    events.add(new EventDay(c, R.drawable.temperature));
                }
                if (type == Type.OVULATION){
                    events.add(new EventDay(c, R.drawable.flower));
                }
            }
            else if (currentEvents.get(day).size() == 2) {
                Iterator<Type> iterator = currentEvents.get(day).iterator();
                Type firstType = iterator.next();
                Type secondType = iterator.next();
                if ((firstType == Type.NOTE && secondType == Type.PERIOD) || (firstType == Type.PERIOD && secondType == Type.NOTE)) {
                    events.add(new EventDay(c, R.drawable.note_period));
                }
                else if ((firstType == Type.NOTE && secondType == Type.OVULATION) ||
                        (firstType == Type.OVULATION && secondType == Type.NOTE)) {
                    events.add(new EventDay(c, R.drawable.note_flower));
                }
                else if ((firstType == Type.NOTE && secondType == Type.TEMPERATURE) || (firstType == Type.TEMPERATURE && secondType == Type.NOTE)) {
                    events.add(new EventDay(c, R.drawable.note_temp));
                }
                else if ((firstType == Type.NOTE && secondType == Type.WEIGHT) || (firstType == Type.WEIGHT && secondType == Type.NOTE)) {
                    events.add(new EventDay(c, R.drawable.note_weight));
                }
                else if ((firstType == Type.WEIGHT && secondType == Type.OVULATION) || (firstType == Type.OVULATION && secondType == Type.WEIGHT)) {
                    events.add(new EventDay(c, R.drawable.weight_ovulation));
                }
                else if ((firstType == Type.WEIGHT && secondType == Type.TEMPERATURE) || (firstType == Type.TEMPERATURE && secondType == Type.WEIGHT)) {
                    events.add(new EventDay(c, R.drawable.temp_weight));
                }
                else if ((firstType == Type.WEIGHT && secondType == Type.PERIOD) || (firstType == Type.PERIOD && secondType == Type.WEIGHT)) {
                    events.add(new EventDay(c, R.drawable.period_weight));
                }
                else if ((firstType == Type.TEMPERATURE && secondType == Type.OVULATION) || (firstType == Type.OVULATION && secondType == Type.TEMPERATURE)) {
                    events.add(new EventDay(c, R.drawable.temp_ovulation));
                }
                else if ((firstType == Type.TEMPERATURE && secondType == Type.PERIOD) || (firstType == Type.PERIOD && secondType == Type.TEMPERATURE)) {
                    events.add(new EventDay(c, R.drawable.period_temp));
                }
            }
            else {//(currentEvents.get(day).size() >= 3){
                events.add(new EventDay(c, R.drawable.dots));
            }

        }
        return events;
    }

    private long calculateOvulation(long startPeriod, int cycleLength) {
        return startPeriod + (cycleLength - 14) * 1000 * 60 * 60 * 24;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);
        activity = (TabbedActivity) getActivity();

        calendarView = view.findViewById(R.id.calendarView);
        mViewModel = ViewModelProviders.of(this).get(MyCalendarViewModel.class);


        activity.addUserSelectionChangedListener(user -> {
            updateEvents(user);
            calendarView.setEvents(convertEvents());
        });

        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {

                Calendar clickedDayCalendar = eventDay.getCalendar();
                Intent i = new Intent();
                Date selectedDate = eventDay.getCalendar().getTime();
                DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
                i.putExtra("timestamp", selectedDate.getTime());
                i.putExtra("selectedDate", format.format(selectedDate));

                i.setClass(view.getContext(), NoteActivity.class);
                i.putExtra("userId", activity.getCurrentUser().getId());
                view.getContext().startActivity(i);
            }
        });

        mViewModel.getAllNotesFromAllUsers().observe(this, notes -> {
            if (notes != null) {
                noteEvents.clear();
                noteEvents.addAll(notes);
            }
            updateEvents(activity.getCurrentUser());
            calendarView.setEvents(convertEvents());

        });

        mViewModel.getAllPeriodsFromAllUsers().observe(this, periods -> {
            if (periods != null) {
                periodEvents.clear();
                periodEvents.addAll(periods);
            }
            updateEvents(activity.getCurrentUser());
            calendarView.setEvents(convertEvents());
        });

        mViewModel.getAllWeights().observe(this, weights -> {
            if (weights != null) {
                weightEvents.clear();
                weightEvents.addAll(weights);
            }
            updateEvents(activity.getCurrentUser());
            calendarView.setEvents(convertEvents());
        });

        mViewModel.getAllTemperatures().observe(this, temperatures -> {
            if (temperatures != null) {
                tempEvents.clear();
                tempEvents.addAll(temperatures);
            }
            updateEvents(activity.getCurrentUser());
            calendarView.setEvents(convertEvents());

        });

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

}
