package at.marinica.mycalendar;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

import at.marinica.mycalendar.database.DateUtil;
import at.marinica.mycalendar.database.MyCalendarViewModel;
import at.marinica.mycalendar.database.Period;
import at.marinica.mycalendar.database.User;

public class StatisticFragment extends Fragment{

    private static final String TAG = "StatisticFragment";

    private MyCalendarViewModel myCalendarViewModel;
    private GraphView graph;
    private BarGraphSeries<DataPoint> series;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_statistic, container, false);

        myCalendarViewModel = ViewModelProviders.of(this).get(MyCalendarViewModel.class);

        TabbedActivity tabbedActivity = (TabbedActivity) getActivity();
        if (tabbedActivity != null) {
            tabbedActivity.addUserSelectionChangedListener(this::update);
        }

        myCalendarViewModel.getAllPeriodsFromAllUsers().observe(this, periods -> {
            if (tabbedActivity != null) {
                update(tabbedActivity.getCurrentUser());
            }
        });

        graph = (GraphView) view.findViewById(R.id.statisticgraph);
        graph.getGridLabelRenderer().setVerticalLabelsVisible(false);

        //graph.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.VERTICAL);
        graph.getGridLabelRenderer().setNumHorizontalLabels(12);
        graph.getGridLabelRenderer().setNumVerticalLabels(10);
        graph.getGridLabelRenderer().setPadding(60);
        graph.getGridLabelRenderer().setHorizontalLabelsAngle(90);

        graph.getGridLabelRenderer().setVerticalAxisTitle("Days");
        final int offset = 1;
        series = new BarGraphSeries<>(new DataPoint[0]);
        graph.addSeries(series);

        graph.getViewport().setMinY(0);
        graph.getViewport().setMaxY(60);
        graph.getViewport().setYAxisBoundsManual(true);

        series.setSpacing(40);

        // draw values on top
        series.setDrawValuesOnTop(true);
        series.setValuesOnTopColor(Color.BLACK);

        return view;

    }

    private void update(User newUser) {
        Log.d(TAG, "entered update");
        if (newUser == null) {
            Log.d(TAG, "newUser is null");
            return;
        }
        List<Period> lastPeriods = myCalendarViewModel.getLastPeriodsForUser(newUser.getId(), 9);
        if (lastPeriods == null) {
            return;
        }
        if (lastPeriods.size() < 2) {
            series.resetData(new DataPoint[0]);
            return;
        }
        lastPeriods.sort(Comparator.comparingLong(Period::getStartDate));
        String[] labels = new String[lastPeriods.size()];
        DataPoint[] data = new DataPoint[lastPeriods.size()];
        for (int i = 0; i < lastPeriods.size(); i++) {
            Period p = lastPeriods.get(i);
            long endDate;
            if (i + 1 < lastPeriods.size()) {
                endDate = lastPeriods.get(i+1).getStartDate();
            }
            else {
                endDate = DateUtil.truncateTimestamp(System.currentTimeMillis());
            }
            long cycleLength = (endDate - p.getStartDate()) / (1000  * 60 * 60 * 24);
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(p.getStartDate());
            String label = getMonthString(cal.get(Calendar.MONTH)) + ". " + cal.get(Calendar.YEAR);
            labels[i] = label;
            data[i] = new DataPoint(i + 1, cycleLength);
            Log.d(TAG, String.format("Found period: id %d, userId %d, startDate %d, endDate %d", p.getId(), p.getUserId(), p.getStartDate(), p.getEndDate()));
            Log.d(TAG, "Cycle length: " + cycleLength);
            Log.d(TAG, "Cycle month: " + label);
        }
        // use static labels for horizontal and vertical labels
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(labels);
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        series.resetData(data);
    }

    private String getMonthString(int month) {
        switch (month) {
            case 0:
                return "JAN";
            case 1:
                return "FEB";
            case 2:
                return "MAR";
            case 3:
                return "APR";
            case 4:
                return "MAY";
            case 5:
                return "JUN";
            case 6:
                return "JUL";
            case 7:
                return "AUG";
            case 8:
                return "SEP";
            case 9:
                return "OCT";
            case 10:
                return "NOV";
            case 11:
                return "DEC";
            default:
                return "";
        }
    }
}
